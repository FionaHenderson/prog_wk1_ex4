﻿using System;

namespace Exercise4
{
    class MainClass
    {
        static void Main(string[] args)
        {
            var a = 8;
            var b = 13;
            var answer = a + b;

            Console.WriteLine(answer);
            Console.WriteLine($"The answer to {a} plus {b} is {answer}");
            
        }
    }
}
